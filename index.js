const express = require("express");
const handlebars = require("express-handlebars");
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const passport = require("passport");
const app = express();
const session = require("express-session");
const redis = require('redis');
const connectRedis = require('connect-redis');

// get environment variables from .env file
require("dotenv").config({ silent: true });
const port = process.env.PORT || 6800;

// set html template engine
app.set("view engine", "handlebars");
app.engine(
    "html",
    handlebars({
        defaultLayout: "index",
        extname: ".html",
        layoutsDir: __dirname + "/app/templates",
        partialsDir: __dirname + "/app/templates/partials",
    })
);

// set html templates directory
app.set("views", __dirname + "/app/templates");
app.set("view cache", false);

// set our assets directory
app.use("/static", express.static(__dirname + "/assets"));

// setup sessions for persistant auth

// Configure redis store
const RedisStore = connectRedis(session);
const redisClient = redis.createClient({
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD,
    port: process.env.REDIS_PORT
});

//Configure session middleware
app.use(
    session({
        store: new RedisStore({ client: redisClient }),
        secret: "secret$%^134",
        resave: false,
        saveUninitialized: false,
        cookie: {
            secure: false, // if true only transmit cookie over https
            httpOnly: false, // if true prevent client side JS from reading the cookie
            maxAge: 1000 * 60 * 10, // session max age in miliseconds
        },
    })
);

// setup passport js for third party authentication
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

passport.use(
    new GoogleStrategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: "https://project-alpha-gilt.vercel.app/auth/google/callback",
        },
        function (accessToken, refreshToken, profile, done) {
            userProfile = profile;
            return done(null, userProfile);
        }
    )
);

// capture request data and parameters
var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});

// import routes
app.use(require("./app/routes/index"));


// page not found
app.get("*", function (req, res) {
    res.render("404.html");
});