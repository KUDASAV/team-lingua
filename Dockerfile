FROM node:10

# create and move into project directory
RUN mkdir /project
WORKDIR /project

# Install node js dependencies
COPY package*.json ./

# Copy project source
COPY . .

# Install dependencies
RUN npm install