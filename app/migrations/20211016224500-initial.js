"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        // comunity load
        await queryInterface.createTable("community_engagement_data", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            outreach_activity: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            comments: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            },
        });

        // courses
        await queryInterface.createTable("courses", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            name: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            programme_code: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            },
            last_updated: {
                type: Sequelize.DATE,
                allowNull: true,
            },
        });

        // reaserch load
        await queryInterface.createTable("reaserch_load_data", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            name: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            programme_code: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            },
            last_updated: {
                type: Sequelize.DATE,
                allowNull: true,
            },
        });

        // supporting documents
        await queryInterface.createTable("supporting_documents", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            data_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            document_name: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
        });

        // teaching load table
        await queryInterface.createTable("teaching_load_data", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            year: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            students_enrolled: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            semester: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            course: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            course_level: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            comments: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
        });

        // user courses, many to many
        await queryInterface.createTable("user_courses", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            course_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            session_type: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            practical_groups: {
                type: Sequelize.INTEGER,
            },
            user_coordinating:{
                type: Sequelize.INTEGER,
            },
            first_time_instructing:{
                type: Sequelize.INTEGER,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            },
        });

        // users
        await queryInterface.createTable("users", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            first_name: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            last_name: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            phone: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            email: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            role: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            },
            last_updated: {
                type: Sequelize.DATE,
                allowNull: true,
            },
        });

        // workload file
        await queryInterface.createTable("workload_file", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            state: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            name: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            },
            last_updated: {
                type: Sequelize.DATE,
                allowNull: true,
            },
        });
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    },
};
