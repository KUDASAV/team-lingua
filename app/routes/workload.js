const express = require('express')
const router = express.Router();
const Models = require("../models")

// courses
router.get('/', async function (req, res) {
    // get all coursesfrom the db
    let worksheet_files = await Models.workload_file.findAll({
        where: { user_id: 1 },
    });
    console.log(worksheet_files)
    res.render('staff_home.html', {
        body_class: "login_page",
        session: req.session,
        worksheets: worksheet_files,
        page_title: "Add Staff",
    })
})

router.post('/', async function (req, res) {
    req.body.state = "Draft"

    try{
        await Models.workload_file.create(req.body);
        return res.redirect("/workload")
    }catch(e){
        console.log(e)
        return res.redirect("/workload")
    }
})

// courses
router.get('/:id', async function (req, res) {

    console.log(req.session)
    // get all coursesfrom the db
    let worksheet = await Models.workload_file.findAll({
        where: { id: 1 },
    });

    courses = await Models.user_courses.findAll({
        where: { user_id: req.session.user_details.id },
    });

    res.render('workload_sheet.html', {
        body_class: "login_page",
        session: req.session,
        courses: courses,
        worksheet: worksheet,
        page_title: "Add Staff",
    })
})

module.exports = router