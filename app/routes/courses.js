const express = require('express')
const router = express.Router();
const Models = require("../models")

// courses
router.get('/', async function (req, res) {
    // get all coursesfrom the db
    let courses = await Models.courses.findAll();
    
    res.render('courses.html', {
        body_class: "login_page",
        session: req.session,
        courses: courses,
        page_title: "Add Staff",
    })
})

router.post('/', async function (req, res) {
    try{
        await Models.courses.create(req.body);
        return res.redirect("/courses")
    }catch(e){
        console.log(e)
        return res.redirect("/courses")
    }
})

module.exports = router