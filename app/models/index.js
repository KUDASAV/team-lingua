const Sequelize = require("sequelize");

const sequelize = new Sequelize(process.env.DATABASE, process.env.DATABASE_USER, process.env.DATABASE_PASSWORD, {
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT, 
    dialect: "mysql",
    dialectModule: require('mysql2'),
    operatorsAliases: false,
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

// import database models
models = [
    'community_engagement_data',
    'courses',
    'reaserch_load_data',
    'supporting_documents',
    'teaching_load_data',
    'user_courses',
    'users',
    'workload_file'
]

models.forEach(model => {
    db[model] = require("./"+model)(sequelize, Sequelize.DataTypes)
});

// create assiciations
for (const i in db) {
    try{
        db[i].associate(db)
    }catch(e){}
}

module.exports = db;