const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    table = sequelize.define(
        "user_courses",
        {
            id: {
                autoIncrement: true,
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
            },
            user_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            course_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            session_type: {
                type: DataTypes.STRING(255),
                allowNull: true,
            },
            practical_groups: {
                type: DataTypes.INTEGER,
            },
            user_coordinating:{
                type: DataTypes.INTEGER,
            },
            first_time_instructing:{
                type: DataTypes.INTEGER,
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            }
        },
        {
            sequelize,
            tableName: "user_courses",
            timestamps: false,
            indexes: [
                {
                    name: "PRIMARY",
                    unique: true,
                    using: "BTREE",
                    fields: [{ name: "id" }],
                },
            ],
        }
    );

    return table
};
